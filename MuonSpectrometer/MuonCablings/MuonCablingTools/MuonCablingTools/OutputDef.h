/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef MUONCABLINGTOOLS_OUTPUTDEF_H
#define MUONCABLINGTOOLS_OUTPUTDEF_H

#include <sstream>
typedef std::ostringstream __osstream;
#define COUNT str().size()

#endif
