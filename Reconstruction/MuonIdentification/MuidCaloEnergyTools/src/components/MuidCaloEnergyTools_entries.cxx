#include "MuidCaloEnergyTools/MuidCaloEnergyMeas.h"
#include "MuidCaloEnergyTools/MuidCaloEnergyParam.h"
#include "MuidCaloEnergyTools/MuidCaloEnergyTool.h"

DECLARE_COMPONENT(Rec::MuidCaloEnergyTool)
DECLARE_COMPONENT(Rec::MuidCaloEnergyMeas)
DECLARE_COMPONENT(Rec::MuidCaloEnergyParam)
